FROM igwn/base:el8

LABEL name="LIGO - Koji Testing - EL8 Production"
LABEL maintainer="Adam Mercer <adam.mercer@ligo.org>"
LABEL support="Best Effort"

# remove upstream igwn repositories
RUN dnf -y remove igwn-production-config \
        igwn-backports-config && \
    dnf clean all

# add igwn-backports repository
RUN echo "[igwn-backports]" > /etc/yum.repos.d/backports.repo && \
    echo "name = igwn-backports" >> /etc/yum.repos.d/backports.repo && \
    echo "baseurl = https://koji.ligo-la.caltech.edu/kojifiles/repos-dist/epel8-lscsoft-backports/latest/x86_64/" >> /etc/yum.repos.d/backports.repo && \
    echo "enabled = 1" >> /etc/yum.repos.d/backports.repo && \
    echo "gpgcheck = 0" >> /etc/yum.repos.d/backports.repo

# add igwn-production repository
RUN echo "[igwn-production]" > /etc/yum.repos.d/production.repo && \
    echo "name = igwn-production" >> /etc/yum.repos.d/production.repo && \
    echo "baseurl = https://koji.ligo-la.caltech.edu/kojifiles/repos-dist/epel8-lscsoft/latest/x86_64/" >> /etc/yum.repos.d/production.repo && \
    echo "enabled = 1" >> /etc/yum.repos.d/production.repo && \
    echo "gpgcheck = 0" >> /etc/yum.repos.d/production.repo

# add igwn-warehouse repository
RUN echo "[igwn-warehouse]" > /etc/yum.repos.d/warehouse.repo && \
    echo "name = igwn-warehouse" >> /etc/yum.repos.d/warehouse.repo && \
    echo "baseurl = https://koji.ligo-la.caltech.edu/kojifiles/repos-dist/epel8-lscsoft-warehouse/latest/x86_64/" >> /etc/yum.repos.d/warehouse.repo && \
    echo "enabled = 1" >> /etc/yum.repos.d/warehouse.repo && \
    echo "gpgcheck = 0" >> /etc/yum.repos.d/warehouse.repo

# install extra packages
RUN dnf -y install \
        bash-completion \
        epel-release && \
    dnf -y upgrade && \
    dnf -y install igwn-packaging-test && \
    dnf clean all

# add commonly used commands to .bash_history
RUN echo "dnf clean all && dnf -y update" > /root/.bash_history
